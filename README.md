Two NodeJS services that communicate with each other. On service is exposed to the public while the other is only accessible internally.

**Front Office Service**

> `GET /`  
`POST /`  
`GET /inventory-service`  
`POST /inventory-service`  
`GET /readiness`

**Inventory Service**

>`GET /api/v1/inventory`  
`POST /api/v1/inventory`

Used in Part 2 of an introductory blog series `Building a Web Service with Minikube` (in-progress)

#### To run locally

`npm start`

Navigate to `front-office`: `localhost:3000`  
Navigate to `inventory-service`: `localhost:3100`

#### To run w/ Docker

Build image `front-office` within `/frontOfficeService` folder  
`docker build -t front-office .`  
`docker run -p 3000:3000 -d front-office`

Build image `inventory-service` within `/inventoryService` folder  
`docker build -t inventory-service .`  
`docker run -p 3100:3100 -d inventory-service`

#### To run w/ Kubernetes

We show two examples of running the services:

1.  One pod with two containers  
    * `cd` into the `manifest` directory
    * `kk apply -f basic-one-pod-service.yaml`
2.  Two pods with one container each
    * `cd` into the `manifest` directory 
    * `kk apply -f basic-two-pod-service.yaml`


#### Resources
* https://docs.docker.com/network/bridge/#differences-between-user-defined-bridges-and-the-default-bridge
* https://stackoverflow.com/questions/53992384/how-to-access-docker-containers-internal-ip-address-from-host
* https://github.com/BretFisher/node-docker-good-defaults/blob/master/Dockerfile
* https://github.com/BretFisher/node-docker-good-defaults/blob/69c923bc646bc96003e9ada55d1ec5ca943a1b19/bin/www
* https://github.com/RisingStack/kubernetes-graceful-shutdown-example/blob/master/src/index.js
* https://kubernetes.io/docs/concepts/workloads/pods/pod-overview/
